This routine calculates 'pythagorean triples' such an integer
arrays (a, b, c) that sum of squires of a and b is equals
to square of c. The calculations are bounded by specifying a
max value for c.
