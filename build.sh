#!/usr/bin/env bash

if [[ -e 'target' ]]; then rm -r 'target'; fi
mkdir 'target'
cp ./run.sh target
cp cmake-build-debug/pythagorean_triples target
cp -r ./resources target

cd target
chmod a+x run.sh
chmod a+x pythagorean_triples


